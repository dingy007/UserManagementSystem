/* import { Employee } from './../models/employee';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EmployeeService {
  clientsRef: AngularFireList<any>;
  clients: Observable<any[]>;
  client: Observable<any>;
  constructor(private db: AngularFireDatabase) {
    this.clientsRef = this.db.list('clients');
    this.clients = this.clientsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
    console.log('Received clients', this.clients);
  }

  getClients() {
    return this.clients;
  }

  newClient(client: Employee) {
    this.clientsRef.push(client);
  }

  getClient(id: string) {
    this.client = this.db.object('/clients/' + id).valueChanges();
    return this.client;
  }
}
 */
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import { Employee } from '../models/Employee';

@Injectable()
export class EmployeeService {
  employeesCollection: AngularFirestoreCollection<Employee>;
  employeeDoc: AngularFirestoreDocument<Employee>;
  employees: Observable<Employee[]>;
  employee: Observable<Employee>;

  constructor(private angularFireStore: AngularFirestore) {
    this.employeesCollection = this.angularFireStore.collection('clients', ref => ref.orderBy('lastName', 'asc'));
  }

  getClients(): Observable<Employee[]> {
    // Get clients with the id
    this.employees = this.employeesCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Employee;
        data.id = action.payload.doc.id;
        return data;
      });
    });

    return this.employees;
  }

  newClient(employee: Employee) {
    this.employeesCollection.add(employee);
  }

  getClient(id: string): Observable<Employee> {
    this.employeeDoc = this.angularFireStore.doc<Employee>(`clients/${id}`);
    this.employee = this.employeeDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Employee;
        data.id = action.payload.id;
        return data;
      }
    });

    return this.employee;
  }

  updateClient(employee: Employee) {
    this.employeeDoc = this.angularFireStore.doc(`clients/${employee.id}`);
    this.employeeDoc.update(employee);
  }

  deleteClient(employee: Employee) {
    this.employeeDoc = this.angularFireStore.doc(`clients/${employee.id}`);
    this.employeeDoc.delete();
  }

}
