import { FlashMessagesModule } from 'angular2-flash-messages';
import { EmployeeService } from './services/employee.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { DashboardComponent } from './mainApp/components/dashboard/dashboard.component';
import { EmployeesComponent } from './mainApp/components/employees/employees.component';
import { EmployeeDetailsComponent } from './mainApp/components/employee-details/employee-details.component';
import { AddEmployeeComponent } from './mainApp/components/add-employee/add-employee.component';
import { EditEmployeeComponent } from './mainApp/components/edit-employee/edit-employee.component';
import { NavbarComponent } from './mainApp/components/navbar/navbar.component';
import { SidebarComponent } from './mainApp/components/sidebar/sidebar.component';
import { LoginComponent } from './mainApp/components/login/login.component';
import { RegisterComponent } from './mainApp/components/register/register.component';
import { SettingsComponent } from './mainApp/components/settings/settings.component';
import { PageNotFoundComponent } from './mainApp/components/page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './services/auth.service';
import { WorkModule } from './workModule/work/work.module';
import { AdminModule } from './adminModule/admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EmployeesComponent,
    EmployeeDetailsComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    NavbarComponent,
    SidebarComponent,
    LoginComponent,
    RegisterComponent,
    SettingsComponent,
    PageNotFoundComponent,
    // AdminComponent,
    // WorkComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule,
    WorkModule,
    AdminModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'clientpanelapp'),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    EmployeeService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
