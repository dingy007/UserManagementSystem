import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './mainApp/components/dashboard/dashboard.component';
import { LoginComponent } from './mainApp/components/login/login.component';
import { RegisterComponent } from './mainApp/components/register/register.component';
import { AddEmployeeComponent } from './mainApp/components/add-employee/add-employee.component';
import { EditEmployeeComponent } from './mainApp/components/edit-employee/edit-employee.component';
import { SettingsComponent } from './mainApp/components/settings/settings.component';
import { PageNotFoundComponent } from './mainApp/components/page-not-found/page-not-found.component';
import { EmployeeDetailsComponent } from './mainApp/components/employee-details/employee-details.component';
import { AuthGuard } from './mainApp/guard/auth.guard';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'employee/add', component: AddEmployeeComponent, canActivate: [AuthGuard] },
  { path: 'employee/edit/:id', component: EditEmployeeComponent, canActivate: [AuthGuard] },
  { path: 'employee/:id', component: EmployeeDetailsComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ],
  providers: [AuthGuard],
  declarations: []
})
export class AppRoutingModule { }
