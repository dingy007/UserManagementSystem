import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkRoutingModule, routedComponents } from './work-routing.module';
import { WorkService } from '../services/work.service';

@NgModule({
  imports: [
    CommonModule,
    WorkRoutingModule
  ],
  declarations: [
    routedComponents
  ],
  providers: [
    WorkService
  ]
})
export class WorkModule { }
