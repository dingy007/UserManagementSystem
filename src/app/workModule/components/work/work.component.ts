import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css'
]
})
export class WorkComponent implements OnInit {
  cat01: string;
  cat02: string;
  cat03: string;
  cat04: string;
  cat05: string;

  constructor() { }


  ngOnInit() {
    this.cat01 = '/assets/images/cat01.jpeg';
    this.cat02 = '/assets/images/cat02.jpeg';
    this.cat03 = '/assets/images/cat03.jpeg';
    this.cat04 = '/assets/images/cat04.jpeg';
    this.cat05 = '/assets/images/cat05.jpeg';
  }

}
