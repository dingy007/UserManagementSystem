import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../services/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  clients: any[];
  totalOwed: number;

  constructor(private empService: EmployeeService) { }

  ngOnInit() {
    this.empService.getClients().subscribe(clients => {
      // this.clients = clients;
      console.log(clients);
      this.clients = clients;
      // this.getTotalOwed();
    });
  }

/*   getTotalOwed() {
    let total = 0;
    for (let i = 0; i < this.clients.length; i++) {
      total += parseFloat(this.clients[i].balance);
    }
    this.totalOwed = total;
  } */
/*
  getTotalOwed() {
    this.clients.reduce((total, client) => {
      return total + client.balance;
    }, 0);
  } */
}
