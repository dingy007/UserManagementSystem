import { EmployeeService } from '../../../services/employee.service';
import { Employee } from '../../../models/employee';
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  client: Employee = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    role: ''
  };

  constructor(
    private flashMessagesService: FlashMessagesService,
    private router: Router,
  private empService: EmployeeService) { }

  ngOnInit() {

  }

  onSubmit({ value, valid }: { value: Employee, valid: boolean }) {
    console.log(value, valid);
    if (!valid) {
      this.flashMessagesService.show('Please fill in all the fields', {
        cssClass: 'alert-danger', timeout: 4000
      });
      this.router.navigate(['add-employee']);
    } else {
      console.log('Successfully completed that.');
      this.empService.newClient(value);

      this.flashMessagesService.show('New employee Added.', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/']);
    }
  }

}

