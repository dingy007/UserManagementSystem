import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../models/employee';
import { FlashMessagesService } from 'angular2-flash-messages';
import { EmployeeService } from '../../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-client-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  id: string;
  client: Employee;
  showBalanceUpdateInput = false;

  constructor(
    private empService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    // get id from url
    this.id = this.route.snapshot.params['id'];
    this.empService.getClient(this.id).subscribe(client => {
      // get client
      this.client = client;
      console.log(client);
    });
  }
  onDeleteClick() {
    if (confirm('Do you want to delete this User?')) {
      this.flashMessagesService.show('Employee removed', { cssClass: 'alert-success', timeout: 4000 });
      this.empService.deleteClient(this.client);
      this.router.navigate(['/']);
    }
  }
}
