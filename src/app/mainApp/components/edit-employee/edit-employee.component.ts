import { Employee } from './../../../models/employee';
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { EmployeeService } from '../../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  id: string;
  employee: Employee = {
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  };
  constructor(
    private empService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
        // get id from url
        this.id = this.route.snapshot.params['id'];
        this.empService.getClient(this.id).subscribe(employee => {
          // get client
          this.employee = employee;
          console.log(employee);
        });
  }

  onSubmit({value, valid}: {value: Employee, valid: boolean}) {
    console.log('Value: ', value, 'Valid: ', valid);
    if (!valid) {
      this.flashMessagesService.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000});
    } else {
      // Add id to client
      value.id = this.id;
      // update client
      this.empService.updateClient(value);
      this.router.navigate(['/employee/' + this.id]);
    }
  }

}
