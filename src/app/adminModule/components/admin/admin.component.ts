import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  dog01: string;
  dog02: string;
  dog03: string;
  dog04: string;
  dog05: string;

  constructor() { }


  ngOnInit() {
    this.dog01 = '/assets/images/dog01.jpeg';
    this.dog02 = '/assets/images/dog02.jpeg';
    this.dog03 = '/assets/images/dog03.jpeg';
    this.dog04 = '/assets/images/dog04.jpeg';
    this.dog05 = '/assets/images/dog05.jpeg';
  }
}
