// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBkGbSz8HafDqejLX6eDcCGz2faiDpGqk0',
    authDomain: 'usermanagementsystembe.firebaseapp.com',
    databaseURL: 'https://usermanagementsystembe.firebaseio.com',
    projectId: 'usermanagementsystembe',
    storageBucket: 'usermanagementsystembe.appspot.com',
    messagingSenderId: '796654513419'

  }
};
